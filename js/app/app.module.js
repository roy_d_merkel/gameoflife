﻿'use strict';

//
//  app.module.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Define the `GameOfLife` module
var settings = {
    isLive: (location.hostname.indexOf('www.') >= 0),
    isStaging: (location.hostname.indexOf('staging.') >= 0),
    isQa: (location.hostname.indexOf('qa.') >= 0),
    isDev: (location.hostname.indexOf('dev.') >= 0),
    devApi: false,
    lang:'en',
    productionApi: false,
    translation:false,
    // default api
    api: './api',
    // see https://code.google.com/p/crypto-js/#The_Hasher_Algorithms
    encryption: 'SHA256',
    _transformRequest: function (data) {
        if (data === undefined)
            return data;
        var clonedData = $.extend(true, {}, data);
        for (var property in clonedData)
            if (property.substr(0, 1) == '$')
                delete clonedData[property];

        return $.param(clonedData);
    }
}

angular.module('gameOfLifeApp', ['ui.router', 'ngStorage', 'WorldState.Controllers', 'Login.Controllers', 'Class.Controllers', 'Hud.Controllers']);

angular.module('gameOfLifeApp')
       .factory('appConfig', [ '$localStorage', function($localStorage){
            var old = {};
            var appConfig = $.extend(true, settings, old);
            appConfig.set = function(prop,val){
              appConfig[prop]=val;
            };
            appConfig._getSessionToken = function(requestConfig) {
                return $localStorage.token;
            };
            return appConfig;
       }]);
