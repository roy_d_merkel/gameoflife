﻿'use strict';

//
//  app.states.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

angular.module('gameOfLifeApp')
       .config(function($stateProvider, $urlRouterProvider) {
                $stateProvider
                    .state('login', {
                                url: '/login',
                                templateUrl: 'templates/app/components/login/login.html',
                                controller: 'LoginCtrl' })
                    .state('hud', {
                                url: '/hud',
                                templateUrl: 'templates/app/components/hud/hud.html',
                                controller: 'HudCtrl' });
                $urlRouterProvider.otherwise('/login');
                });