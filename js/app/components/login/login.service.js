﻿'use strict';

//
//  login.service.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
angular.
  module('Login.Services', ['ngResource', 'ngStorage']).
  service('LoginService',
    function($resource, $localStorage, appConfig) {
      return $resource(appConfig.api + '/login', { }, {
        save: {
          method: 'POST',
          hasBody: true,
        },
        is_logged_in: {
          method: 'GET',
          hasBody: false,
          headers: { token: appConfig._getSessionToken },
          url: appConfig.api + '/is_logged_in',
        }
      }
      //uncomment to use query params of "a" and "b" (params)
      //and to convert the post body to url encoded standard from json (transformRequest)
      //return $resource(appConfig.api + '/login', { }, {
      //  save: {
      //    method: 'POST',
      //    transformRequest: appConfig._transformRequest,
      //    headers: {
      //      'Content-Type': 'application/x-www-form-urlencoded'
      //    },
      //    params: { a: 'a', b:'b' },
      //    hasBody: true,
      //  }
      //}
      );
    }
  );