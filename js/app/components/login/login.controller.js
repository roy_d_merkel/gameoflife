﻿'use strict';

//
//  login.controller.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
angular.module('Login.Controllers', ['ngResource', 'ngStorage', 'Login.Services', 'Class.Services', 'Hud.Controllers'])
       .controller('LoginCtrl', function($scope, $state, $localStorage, LoginService, ClassService, appConfig) {
        ClassService.queryAll({}, function(data, getHeadersFunc, status) {
                                $scope.classes = data;

                                $scope.class_id = $scope.classes[0].class_id;
                              });

        $scope.login = function() {
            var cryptMethod = appConfig.encryption;
            var passHash = CryptoJS[cryptMethod]($scope.password);
            var passHashString = passHash.toString(CryptoJS.enc.HEX);
            LoginService.save(  { user_name: $scope.user_name, password: passHashString, class_id: $scope.class_id }, function(data, getHeadersFunc, status) {
                                    if("token" in data && data.token !== undefined)
                                    {
                                        $localStorage.token = data.token;

                                        $state.go('hud');
                                    }
                                }, function(response) {
                                    var status = response.status;
                                    alert('Failed to log in: username, or password incorrect.');
                                });
        };
       });