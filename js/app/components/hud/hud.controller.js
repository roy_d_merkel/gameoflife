﻿'use strict';

//
//  hud.controller.js
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
angular.module('Hud.Controllers', ['ngResource', 'ngStorage', 'Hud.Services', 'WorldState.Services', 'Login.Services'])
       .controller('HudCtrl', function($scope, $state, $localStorage, HudService, WorldStateService, LoginService, appConfig) { 
            $scope.token = $localStorage.token;
            $scope.date = '2018-04-17';

            LoginService.is_logged_in({}, function(data) {
                                                $scope.logged_in = data.logged_in;

                                                if(!$scope.logged_in)
                                                {
                                                    $localStorage.$reset();
                                                    $state.go('login');
                                                }
                                                else
                                                {
                                                    $scope.world_state = WorldStateService.query({ date: $scope.date });
                                                }
                                            });
       });