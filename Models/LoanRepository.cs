﻿//
//  LoanRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class LoanRepository
    {
        public List<Loan> loans(int studentId)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = :1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = @1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("active_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Loan> loans(int studentId, DateTime month)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = ? AND ? BETWEEN active_start AND active_end;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = :1 AND :2 BETWEEN active_start AND active_end;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = @1 AND @2 BETWEEN active_start AND active_end;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("active_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Loan> loans(int studentId, DateTime start, DateTime end)
        {
            List<Loan> result = new List<Loan>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = ? AND active_start <= ? AND active_end >= ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = :1 AND active_start <= :2 AND active_end >= :3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM active_expenses e
WHERE user_id = @1 AND active_start <= @2 AND active_end >= @3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), end), new ParamType(typeof(DateTime), start));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object active_start;
                object active_end;
                object category;
                object total_amount;
                object monthly_amount;

                row.TryGetValue("active_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("active_start", out active_start);
                row.TryGetValue("active_end", out active_end);
                row.TryGetValue("category", out category);
                row.TryGetValue("total_amount", out total_amount);
                row.TryGetValue("monthly_amount", out monthly_amount);

                Loan e = new Loan((int)id, (int?)student_id, (DateTime?)active_start, (DateTime?)active_end, (string)category, (string)total_amount, (string)monthly_amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }
    }
}
