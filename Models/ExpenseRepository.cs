﻿//
//  ExpenseRepository.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class ExpenseRepository
    {
        public List<Expense> expenses(int studentId)
        {
            List<Expense> result = new List<Expense>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = :1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = @1;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;

                row.TryGetValue("past_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);

                Expense e = new Expense((int)id, (int?)student_id, (DateTime)charged_on, (string)category, (string)amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Expense> expenses(int studentId, DateTime month)
        {
            List<Expense> result = new List<Expense>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = ? AND MONTH(charged_on) = MONTH(?);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = :1 AND extract(month from charged_on) = extract(month from :2);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = @1 AND MONTH(charged_on) = MONTH(@2);";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), month));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;

                row.TryGetValue("past_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);

                Expense e = new Expense((int)id, (int?)student_id, (DateTime)charged_on, (string)category, (string)amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }

        public List<Expense> expenses(int studentId, DateTime start, DateTime end)
        {
            List<Expense> result = new List<Expense>();
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = ? AND MONTH(charged_on) BETWEEN ? AND ?;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), start), new ParamType(typeof(DateTime), end));

                    break;
                case "postgresql":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = :1 AND MONTH(charged_on) BETWEEN :2 AND :3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), start), new ParamType(typeof(DateTime), end));

                    break;
                case "sqlserver":
                    query =
@"SELECT *
FROM past_expenses e
WHERE user_id = @1 AND MONTH(charged_on) BETWEEN @2 AND @3;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(DateTime), start), new ParamType(typeof(DateTime), end));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object id;
                object student_id;
                object charged_on;
                object category;
                object amount;

                row.TryGetValue("past_expense_id", out id);
                row.TryGetValue("user_id", out student_id);
                row.TryGetValue("charged_on", out charged_on);
                row.TryGetValue("category", out category);
                row.TryGetValue("amount", out amount);

                Expense e = new Expense((int)id, (int?)student_id, (DateTime)charged_on, (string)category, (string)amount);
                result.Add(e);
            }

            conn.Close();

            return result;
        }
    }
}
