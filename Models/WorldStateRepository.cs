﻿//
//  WorldStateRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class WorldStateRepository
    {
        public WorldState GetWorldState(int classId = -1, Date? date = null)
        {
            DBConnection conn = new DBConnection("GameOfLife");

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT ms.*
FROM WorldState ws
INNER JOIN MarketStrength ms ON ws.market_strength_id = ms.market_strength_id
WHERE (? IS NULL OR ? <= 0 OR ws.class_id = ?) AND (? IS NULL OR ? = CAST('0000-00-00' AS DATE) OR ? BETWEEN ws.date_start AND ws.date_end)";
                    break;
                case "postgresql":
                    query =
@"SELECT ms.*
FROM WorldState ws
INNER JOIN MarketStrength ms ON ws.market_strength_id = ms.market_strength_id
WHERE (:1 IS NULL OR :1 <= 0 OR ws.class_id = :1) AND (:2 IS NULL OR :2 BETWEEN ws.date_start AND ws.date_end)";
                    break;
                case "sqlserver":
                    query =
@"SELECT ms.*
FROM WorldState ws
INNER JOIN MarketStrength ms ON ws.market_strength_id = ms.market_strength_id
WHERE (@1 IS NULL OR @1 <= 0 OR ws.class_id = @1) AND (@2 IS NULL OR @2 BETWEEN ws.date_start AND ws.date_end)";
                    
                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), classId), new ParamType(typeof(Date), date));
            WorldState ws = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object market_strength_id;
                object market_strength;
                object description;

                row.TryGetValue("market_strength_id", out market_strength_id);
                row.TryGetValue("market_strength", out market_strength);
                row.TryGetValue("description", out description);
                ws = new WorldState((int)market_strength_id, (int)market_strength, (string)description);
            }

            conn.Close();

            return ws;
        }
    }
}
