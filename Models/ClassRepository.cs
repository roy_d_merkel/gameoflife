﻿//
//  WorldStateRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class ClassRepository
    {
        public Class[] GetClasses(int classId = -1)
        {
            DBConnection conn = new DBConnection("GameOfLife");

            string query;

            List<Dictionary<string, object>> rs = null;
            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT c.*
FROM Class c
WHERE (? IS NULL OR ? = '' OR ? <= 0 OR c.class_id = ?)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), classId), new ParamType(typeof(int), classId), new ParamType(typeof(int), classId), new ParamType(typeof(int), classId));
                    break;
                case "postgresql":
                    query =
@"SELECT c.*
FROM Class c
WHERE (:1 IS NULL OR :1 = '' OR :1 <= 0 OR c.class_id = :1)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), classId));
                    break;
                case "sqlserver":
                    query =
@"SELECT c.*
FROM Class c
WHERE (@1 IS NULL OR @1 = '' OR @1 <= 0 OR c.class_id = @1)";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), classId));
                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            List<Class> classes = new List<Class>();

            foreach (Dictionary<string, object> row in rs)
            {
                object class_id;
                object teacher_id;
                object name;
                object description;

                row.TryGetValue("class_id", out class_id);
                row.TryGetValue("teacher_id", out teacher_id);
                row.TryGetValue("name", out name);
                row.TryGetValue("description", out description);
                Class cls = new Class((int)class_id, (int)teacher_id, (string)name, (string)description);
                classes.Add(cls);
            }

            conn.Close();

            if (classes != null)
            {
                return classes.ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
