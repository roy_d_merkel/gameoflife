﻿//
//  Events.cs
//
//  Author:
//       John Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 John Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;
using GameOfLife.Entities;
namespace GameOfLife.Models
{
    public class Events
    {
        List<Event> _events;

        public Events()
        {
            bool readHeader = false;
            List<string> columns = new List<string>();
            string[] fields;

            _events = new List<Event>();

            using (TextFieldParser csvParser = new TextFieldParser("Events.csv"))
            {
                csvParser.SetDelimiters(new string[] { "," });
                csvParser.HasFieldsEnclosedInQuotes = true;

                csvParser.ReadLine();
                while (!csvParser.EndOfData)
                {
                    if (!readHeader)
                    {
                        fields = csvParser.ReadFields();
                        columns = new List<string>(fields);
                        continue;
                    }

                    Dictionary<string, string> cols = new Dictionary<string, string>();

                    fields = csvParser.ReadFields();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        if (columns.Count > i)
                        {
                            cols[columns[i]] = fields[i];
                        }
                    }

                    if (fields[0].Trim().Equals(""))
                    {
                        continue;
                    }

                    Event newEvent = new Event(cols["id"], cols["frequency"], cols["probability"], cols["conditions"], cols["description"], cols["sets_flags"], cols["clears_flags"]);
                    _events.Add(newEvent);
                }
            }
        }

        public Event this[string _id]
        {
            get 
            {
                for (int i = 0; i < _events.Count; i++)
                {
                    if(_events[i].id.Equals(_id))
                    {
                        return _events[i];
                    }
                }

                return null;
            }
        }

        public Event this[int n]
        {
            get
            {
                if(_events.Count > n)
                {
                    return _events[n];
                }

                return null;
            }
        }

        public List<Event> events
        {
            get
            {
                return _events;
            }
        }

        public int Count 
        { 
            get 
            { 
                return _events.Count; 
            } 
        }
    }
}
