﻿//
//  LoginRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class SessionRepository
    {
        UserRepository userRepository = null;

        public SessionRepository()
        {
            userRepository = new UserRepository();
        }

        public void insertLoginAttempt(string userName, string password, string ip, string userAgent, string referrer, DateTime attemptedOn, bool success, bool isAdmin)
        {
            DBConnection conn = new DBConnection("GameOfLife");
            int res = -1;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO LoginAttempt(ip, user_agent, referrer, user_name, password, attempted_on, successful, is_admin)
VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(string), ip), new ParamType(typeof(string), userAgent), new ParamType(typeof(string), referrer), new ParamType(typeof(string), userName), new ParamType(typeof(string), password), new ParamType(typeof(DateTime), attemptedOn), new ParamType(typeof(bool), success), new ParamType(typeof(bool), isAdmin));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO LoginAttempt(ip, user_agent, referrer, user_name, password, attempted_on, successful, is_admin)
VALUES (:1, :2, :3, :4, :5, :6, :7, :8)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(string), ip), new ParamType(typeof(string), userAgent), new ParamType(typeof(string), referrer), new ParamType(typeof(string), userName), new ParamType(typeof(string), password), new ParamType(typeof(DateTime), attemptedOn), new ParamType(typeof(bool), success), new ParamType(typeof(bool), isAdmin));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO LoginAttempt(ip, user_agent, referrer, user_name, password, attempted_on, successful, is_admin)
VALUES (@1, @2, @3, @4, @5, @6, @7, @8)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(string), ip), new ParamType(typeof(string), userAgent), new ParamType(typeof(string), referrer), new ParamType(typeof(string), userName), new ParamType(typeof(string), password), new ParamType(typeof(DateTime), attemptedOn), new ParamType(typeof(bool), success), new ParamType(typeof(bool), isAdmin));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            conn.Close();
        }

        public string CreateSessionToken()
        {
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;
            bool exists;
            Random random = new Random();
            string digits = null;
            int numDigits = 64;

            if (numDigits % 2 == 1)
            {
                numDigits++;
            }

            do
            {
                int i = 0;
                digits = "";

                exists = false;

                for (i = 0; i < numDigits / 2 * 2; i += 2)
                {
                    digits += random.Next(0, 0x100).ToString("X2");
                }

                switch (conn.DriverClass)
                {
                    case "mysql":
                        query =
    @"SELECT IF(COUNT(s.session_id) > 0, 1, 0) AS exi
FROM Session s
WHERE s.token = ?";
                        rs = conn.ExecuteQuery(query, new ParamType(typeof(string), digits));

                        break;
                    case "postgresql":
                        query =
    @"SELECT CASE WHEN (COUNT(s.session_id) > 0) THEN 1 ELSE 0 END AS exi
FROM Session s
WHERE s.token = :1";
                        rs = conn.ExecuteQuery(query, new ParamType(typeof(string), digits));

                        break;
                    case "sqlserver":
                        query =
    @"SELECT CASE WHEN (COUNT(s.session_id) > 0) THEN 1 ELSE 0 END AS exi
FROM Session s
WHERE s.token = @1";
                        rs = conn.ExecuteQuery(query, new ParamType(typeof(string), digits));

                        break;
                    default:
                        throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
                }

                foreach (Dictionary<string, object> row in rs)
                {
                    object exi;

                    row.TryGetValue("exi", out exi);

                    exists = ((int)exi != 0);
                }
            } while (exists);

            conn.Close();

            return digits;
        }

        public void insertSession(int studentId, int teacherId, int classId, DateTime startTime, out string token)
        {
            DBConnection conn = new DBConnection("GameOfLife");
            int res = -1;
            string query;

            token = CreateSessionToken();

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"INSERT INTO Session(student_id, teacher_id, class_id, start_time, token)
VALUES (?, ?, ?, ?, ?)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(int), classId), new ParamType(typeof(DateTime), startTime), new ParamType(typeof(string), token));

                    break;
                case "postgresql":
                    query =
@"INSERT INTO Session(student_id, teacher_id, class_id, start_time, token)
VALUES (:1, :2, :3, :4, :5)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(int), classId), new ParamType(typeof(DateTime), startTime), new ParamType(typeof(string), token));

                    break;
                case "sqlserver":
                    query =
@"INSERT INTO Session(student_id, teacher_id, class_id, start_time, token)
VALUES (@1, @2, @3, @4, @5)";
                    res = conn.ExecuteInsert(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(int), classId), new ParamType(typeof(DateTime), startTime), new ParamType(typeof(string), token));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            conn.Close();
        }

        public string DoLogin(HttpSessionState session, string userName, string password, int classId, string ip, string userAgent, string referrer, DateTime attemptedOn)
        {
            IAuthUser user = userRepository.Get(userName, password, classId);

            if (user == null || user.id <= 0)
            {
                insertLoginAttempt(userName, password, ip, userAgent, referrer, attemptedOn, false, false);
                return null;
            }
            else
            {
                string token = null;
                insertLoginAttempt(userName, password, ip, userAgent, referrer, attemptedOn, true, user.is_admin);

                if (user.is_admin)
                {
                    insertSession(-1, user.id, classId, attemptedOn, out token);
                }
                else
                {
                    insertSession(user.id, -1, classId, attemptedOn, out token);
                }

                return token;
            }
        }

        public Session GetSession(string token)
        {
            DBConnection conn = new DBConnection("GameOfLife");

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT s.*
FROM Session s
WHERE s.token = ?;";
                    break;
                case "postgresql":
                    query =
@"SELECT s.*
FROM Session s
WHERE s.token = :1;";
                    break;
                case "sqlserver":
                    query =
@"SELECT s.*
FROM Session s
WHERE s.token = @1;";

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(string), token));
            Session s = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object session_id;
                object student_id;
                object teacher_id;
                object class_id;
                object start_time;
                object end_time;
                object _token;

                row.TryGetValue("session_id", out session_id);
                row.TryGetValue("student_id", out student_id);
                row.TryGetValue("teacher_id", out teacher_id);
                row.TryGetValue("class_id", out class_id);
                row.TryGetValue("start_time", out start_time);
                row.TryGetValue("end_time", out end_time);
                row.TryGetValue("token", out _token);
                s = new Session((int)session_id, (int)student_id, (int)teacher_id, (int)class_id, (DateTime?)start_time, (DateTime?)end_time, (string)_token);
            }

            conn.Close();

            return s;
        }
    }
}
