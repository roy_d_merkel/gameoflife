﻿//
//  UserRepository.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using GameOfLife.Entities;
using T4R.Data;

namespace GameOfLife.Models
{
    public class UserRepository
    {
        public bool userIdExists(string user_name, out int studentId, out int teacherId)
        {
            bool exists = false;
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            studentId = -1;
            teacherId = -1;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.user_name = ?
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.user_name = ?
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(string), user_name), new ParamType(typeof(string), user_name));

                    break;
                case "postgresql":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.user_name = :1
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.user_name = :1
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(string), user_name));

                    break;
                case "sqlserver":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.user_name = @1
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.user_name = @1
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(string), user_name));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object student_id;
                object teacher_id;

                row.TryGetValue("student_id", out student_id);
                row.TryGetValue("teacher_id", out teacher_id);

                if ((int)student_id > 0 || (int)teacher_id > 0)
                {
                    studentId = (int)student_id;
                    teacherId = (int)teacher_id;

                    exists = true;

                    break;
                }
            }

            conn.Close();

            return exists;
        }

        public bool userIdPasswordExists(int studentId, int teacherId, string password)
        {
            bool exists = false;
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.student_id = ? AND s.password = ?
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.teacher_id = ? AND t.password = ?
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(string), password), new ParamType(typeof(int), teacherId), new ParamType(typeof(string), password));

                    break;
                case "postgresql":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.student_id = :1 AND s.password = :3
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.teacher_id = :2 AND t.password = :3
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(string), password));

                    break;
                case "sqlserver":
                    query =
@"SELECT MAX(se.student_id) AS student_id, MAX(se.teacher_id) AS teacher_id
FROM (
    SELECT -1 AS student_id, -1 AS teacher_id
    UNION
    SELECT s.student_id, -1 AS teacher_id
    FROM Students s
    WHERE s.student_id = @1 AND s.password = @3
    UNION 
    SELECT -1 AS student_id, t.teacher_id
    FROM Teachers t
    WHERE t.teacher_id = @2 AND t.password = @3
) AS se;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(string), password));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object student_id;
                object teacher_id;

                row.TryGetValue("student_id", out student_id);
                row.TryGetValue("teacher_id", out teacher_id);

                if ((int)student_id > 0 || (int)teacher_id > 0)
                {
                    exists = true;

                    break;
                }
            }

            conn.Close();

            return exists;
        }

        public bool userIdClassExists(int studentId, int teacherId, ref int classId)
        {
            bool exists = false;
            DBConnection conn = new DBConnection("GameOfLife");
            List<Dictionary<string, object>> rs = null;

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT MAX(q.class_id) AS class_id
FROM
(
    SELECT sc.class_id
    FROM Students s
    INNER JOIN StudentsClasses sc ON s.student_id = sc.student_id
    INNER JOIN Class c ON sc.class_id = c.class_id
    WHERE s.student_id = ? AND sc.class_id = ?
    UNION
    SELECT c.class_id
    FROM Class c
    INNER JOIN Teachers t ON c.teacher_id = t.teacher_id
    WHERE c.class_id = ? AND t.teacher_id = ?
    UNION
    SELECT -1 AS class_id
) AS q;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), classId), new ParamType(typeof(int), classId), new ParamType(typeof(int), teacherId));

                    break;
                case "postgresql":
                    query =
@"SELECT MAX(q.class_id) AS class_id
FROM
(
    SELECT sc.class_id
    FROM Students s
    INNER JOIN StudentsClasses sc ON s.student_id = sc.student_id
    INNER JOIN Class c ON sc.class_id = c.class_id
    WHERE s.student_id = :1 AND sc.class_id = :3
    UNION
    SELECT c.class_id
    FROM Class c
    INNER JOIN Teachers t ON c.teacher_id = t.teacher_id
    WHERE c.class_id = :3 AND t.teacher_id = :2
    UNION
    SELECT -1 AS class_id
) AS q;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(int), classId));

                    break;
                case "sqlserver":
                    query =
@"SELECT MAX(q.class_id) AS class_id
FROM
(
    SELECT sc.class_id
    FROM Students s
    INNER JOIN StudentsClasses sc ON s.student_id = sc.student_id
    INNER JOIN Class c ON sc.class_id = c.class_id
    WHERE s.student_id = @1 AND sc.class_id = @3
    UNION
    SELECT c.class_id
    FROM Class c
    INNER JOIN Teachers t ON c.teacher_id = t.teacher_id
    WHERE c.class_id = @3 AND t.teacher_id = @2
    UNION
    SELECT -1 AS class_id
) AS q;";
                    rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId), new ParamType(typeof(int), teacherId), new ParamType(typeof(int), classId));

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }

            foreach (Dictionary<string, object> row in rs)
            {
                object class_id;

                row.TryGetValue("class_id", out class_id);

                if ((int)class_id > 0)
                {
                    classId = (int)class_id;
                    exists = true;

                    break;
                }
            }

            conn.Close();

            return exists;
        }

        public Student GetStudent(int studentId)
        {
            DBConnection conn = new DBConnection("GameOfLife");

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT s.*
FROM Students s
WHERE s.student_id = ?;";
                    break;
                case "postgresql":
                    query =
@"SELECT s.*
FROM Students s
WHERE s.student_id = :1;";
                    break;
                case "sqlserver":
                    query =
                        @"SELECT s.*
FROM Students s
WHERE s.student_id = @1;";

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), studentId));
            Student s = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object student_id;
                object first_name;
                object last_name;
                object user_name;
                object password;

                row.TryGetValue("student_id", out student_id);
                row.TryGetValue("first_name", out first_name);
                row.TryGetValue("last_name", out last_name);
                row.TryGetValue("user_name", out user_name);
                row.TryGetValue("password", out password);
                s = new Student((int)student_id, (string)first_name, (string)last_name, (string)user_name, (string)password);
            }

            conn.Close();

            return s;
        }

        public Teacher GetTeacher(int teacherId)
        {
            DBConnection conn = new DBConnection("GameOfLife");

            string query;

            switch (conn.DriverClass)
            {
                case "mysql":
                    query =
@"SELECT t.*
FROM Teachers t
WHERE t.teacher_id = ?;";
                    break;
                case "postgresql":
                    query =
@"SELECT t.*
FROM Teachers t
WHERE t.teacher_id = :1;";
                    break;
                case "sqlserver":
                    query =
@"SELECT t.*
FROM Teachers t
WHERE t.teacher_id = @1;";

                    break;
                default:
                    throw new NotSupportedException("driver class: " + conn.DriverClass + " is not supported.");
            }
            List<Dictionary<string, object>> rs = conn.ExecuteQuery(query, new ParamType(typeof(int), teacherId));
            Teacher t = null;

            foreach (Dictionary<string, object> row in rs)
            {
                object teacher_id;
                object first_name;
                object last_name;
                object user_name;
                object password;

                row.TryGetValue("teacher_id", out teacher_id);
                row.TryGetValue("first_name", out first_name);
                row.TryGetValue("last_name", out last_name);
                row.TryGetValue("user_name", out user_name);
                row.TryGetValue("password", out password);
                t = new Teacher((int)teacher_id, (string)first_name, (string)last_name, (string)user_name, (string)password);
            }

            conn.Close();

            return t;
        }

        public IAuthUser Get(string user_name, string password, int classId)
        {
            int studentId = -1;
            int teacherId = -1;

            if (!userIdExists(user_name, out studentId, out teacherId))
            {
                return null;
            }
            else if (!userIdPasswordExists(studentId, teacherId, password))
            {
                return null;
            }
            else if (!userIdClassExists(studentId, teacherId, ref classId))
            {
                return null;
            }
            else if (teacherId > 0)
            {
                return GetTeacher(teacherId);
            }
            else if (studentId > 0)
            {
                return GetStudent(studentId);
            }
            else
            {
                return null;
            }
        }
    }
}
