﻿//
//  Event.cs
//
//  Author:
//       John Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 John Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Event
    {
        private string _id;
        private string _frequency;
        private string _probability;
        private string _conditions;
        private string _description;
        private string[] _sets_flags;
        private string[] _clears_flags;

        public Event(string id, string frequency, string probability, string conditions, string description, string sets_flags_arr_str, string clears_flags_arr_str)
        {
            this._id = id;
            this._frequency = frequency;
            this._probability = probability;
            this._conditions = conditions;
            this._description = description;

            if(!sets_flags_arr_str.Trim().Equals(""))
            {
                this._sets_flags = sets_flags_arr_str.Split(new char[] { ';' });
            }
            else
            {
                this._sets_flags = new string[0];
            }

            if (!clears_flags_arr_str.Trim().Equals(""))
            {
                this._clears_flags = clears_flags_arr_str.Split(new char[] { ';' });
            }
            else
            {
                this._clears_flags = new string[0];
            }
        }

        public string id { get { return _id; } }
        public string probability { get { return _probability; } }
        public string frequency { get { return _frequency; } }
        public string conditions { get { return _conditions; } }
        public string description { get { return _description; } }
        public string[] sets_flags { get { return _sets_flags; } }
        public string[] clears_flags { get { return clears_flags; } }
    }
}
