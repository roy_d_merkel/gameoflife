﻿//
//  Class.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Runtime.Serialization;

namespace GameOfLife.Entities
{
    [DataContractAttribute]
    public class Class
    {
        private int _classId;
        private int _teacherId;
        private string _name;
        private string _description;

        public Class(int classId, int teacherId, string name, string description)
        {
            _classId = classId;
            _teacherId = teacherId;
            _name = name;
            _description = description;
        }

        [DataMemberAttribute] public int class_id { get { return _classId; } set { _classId = value; } }
        [DataMemberAttribute] public int teacher_id { get { return _teacherId; } set { _teacherId = value; } }
        [DataMemberAttribute] public string name { get { return _name; } set { _name = value; } }
        [DataMemberAttribute] public string description { get { return _description; } set { _description = value; } }
    }
}
