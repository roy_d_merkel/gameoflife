﻿//
//  Teacher.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Security.Principal;

namespace GameOfLife.Entities
{
    public class Teacher : IAuthUser
    {
        int _teacherId;
        string _firstName;
        string _lastName;
        string _userName;
        string _password;
        bool _isAdmin;

        public Teacher(int teacherId, string firstName, string lastName, string userName, string password)
        {
            _teacherId = teacherId;
            _firstName = firstName;
            _lastName = lastName;
            _userName = userName;
            _password = password;
            _isAdmin = true;
        }

        public int id { get { return teacher_id; } }
        public int teacher_id { get { return _teacherId; } }
        public string first_name { get { return _firstName; } }
        public string last_name { get { return _lastName; } }
        public string user_name { get { return _userName; } }
        public string password { get { return _password; } }
        public bool is_admin { get { return _isAdmin; } }
    }
}
