﻿//
//  Loan.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Security.Principal;

namespace GameOfLife.Entities
{
    public class Loan
    {
        private int _id;
        private int? _studentId;
        private DateTime? _start;
        private DateTime? _end;
        private string _category;
        private string _totalAmount;
        private string _monthlyAmount;

        public Loan(int id, int? studentId, DateTime? start, DateTime? end, string category, string totalAmount, string monthlyAmount)
        {
            _id = id;
            _studentId = studentId;
            _start = start;
            _end = end;
            _category = category;
            _totalAmount = totalAmount;
            _monthlyAmount = monthlyAmount;
        }

        public int id { get { return _id; } }
        public int? student_id { get { return _studentId; } }
        public DateTime? start { get { return _start; } }
        public DateTime? end { get { return _end; } }
        public string category { get { return _category; } }
        public string totalAmount { get { return _totalAmount; } }
        public string monthlyAmount { get { return _monthlyAmount; } }
    }
}
