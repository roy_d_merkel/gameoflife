﻿//
//  Session.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
namespace GameOfLife.Entities
{
    public class Session
    {
        private int _sessionId;
        private int _studentId;
        private int _teacherId;
        private int _classId;
        private DateTime? _startTime;
        private DateTime? _endTime;
        private string _token;

        public Session(int sessionId, int studentId, int teacherId, int classId, DateTime? startTime, DateTime? endTime, string token)
        {
            _sessionId = sessionId;
            _studentId = studentId;
            _teacherId = teacherId;
            _classId = classId;
            _startTime = startTime;
            _endTime = endTime;
            _token = token;
        }

        public int session_id { get { return _sessionId; } }
        public int student_id { get { return _studentId; } }
        public int teacher_id { get { return _teacherId; } }
        public int class_id { get { return _classId; } }
        public DateTime? start_time { get { return _startTime; } }
        public DateTime? end_time { get { return _endTime; } }
        public string token { get { return _token; } }
    }
}
