﻿//
//  Expense.cs
//
//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Security.Principal;

namespace GameOfLife.Entities
{
    public class Expense
    {
        private int _id;
        private int? _studentId;
        private DateTime? _chargedOn;
        private string _category;
        private string _amount;

        public Expense(int id, int? studentId, DateTime? chargedOn, string category, string amount)
        {
            _id = id;
            _studentId = studentId;
            _chargedOn = chargedOn;
            _amount = amount;
        }

        public int id { get { return _id; } }
        public int? student_id { get { return _studentId; } }
        public DateTime? charged_on { get { return _chargedOn; } }
        public string category { get { return _category; } }
        public string amount { get { return _amount; } }
    }
}
