﻿//
//  World_StateController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;

namespace GameOfLife.Controllers
{
    public class World_StateController : ApiController
    {
        WorldStateRepository worldStateRepository = null;
        public World_StateController()
        {
            this.worldStateRepository = new WorldStateRepository();
        }

        [TokenAuthFilter(true)]
        public HttpResponseMessage Get(HttpRequestMessage request, DateTime? id = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();

            WorldState ws = null;

            UserSessionIdentity identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);
            Session session = identity.session;
            int classId = session.class_id; // will be in session.
            Date? isDt = null;

            if (id != null && id.HasValue)
            {
                isDt = new Date(id.Value.Year, id.Value.Month, id.Value.Day);
            }

            ws = this.worldStateRepository.GetWorldState(classId, isDt);

            if (ws != null)
            {
                return request.CreateResponse(HttpStatusCode.OK, ws);
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.NotFound, ws);
            }
        }
    }
}
