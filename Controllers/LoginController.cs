﻿//
//  LoginController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;
using GameOfLife.App_Start;
using GameOfLife.Entities;
using GameOfLife.Filters;
using GameOfLife.Models;
using Newtonsoft.Json;

namespace GameOfLife.Controllers
{
    public class IsLoggedInResponse
    {
        private bool _isLoggedIn;

        public IsLoggedInResponse(bool isLoggedIn)
        {
            _isLoggedIn = isLoggedIn;
        }

        public bool logged_in { get { return _isLoggedIn; } }
    }

    public class LoginResponse
    {
        private string _token;
        public LoginResponse(string token)
        {
            _token = token;
        }

        public string token { get { return _token; } }
    }

    public class LoginController : ApiController
    {
        SessionRepository sessionRepository = null;

        public LoginController()
        {
            this.sessionRepository = new SessionRepository();
        }

        [TokenAuthFilter(true, false)]
        [System.Web.Http.Route("api/is_logged_in")]
        public HttpResponseMessage Get(HttpRequestMessage request)
        {
            UserSessionIdentity identity = null;
            Session session = null;

            if(Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            return request.CreateResponse(HttpStatusCode.OK, new IsLoggedInResponse(identity != null && session != null && session.session_id > 0));
        }

        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            // handle any other kind of processing here?
            Dictionary<string, dynamic> parms = request.GetParams();

            string user_name = null;
            string password = null;
            int? class_id = null;

            if (parms == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            Dictionary<string, dynamic>.Enumerator enumerator = parms.GetEnumerator();

            while (enumerator.MoveNext())
            {
                KeyValuePair<string, dynamic> kv = enumerator.Current;

                switch (kv.Key)
                {
                    case "user_name":
                        try
                        {
                            user_name = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "password":
                        try
                        {
                            password = (string)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                    case "class_id":
                        try
                        {
                            class_id = (int)kv.Value;
                        }
                        catch (Exception)
                        {
                        }

                        break;
                }
            }

            if (user_name == null || password == null || class_id == null)
            {
                return request.CreateResponse(HttpStatusCode.BadRequest, "No parameters provided!");
            }

            HttpSessionState session = HttpContext.Current.Session;
            string ip = request.GetClientIpAddress();
            string userAgent = request.GetUserAgent();
            string referrer = request.GetReferrer();

            string token = this.sessionRepository.DoLogin(session, user_name, password, class_id.Value, ip, userAgent, referrer, DateTime.Now);

            if (token == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized, new LoginResponse(token));
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.OK, new LoginResponse(token));
            }
        }
    }
}
