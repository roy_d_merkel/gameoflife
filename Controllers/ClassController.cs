﻿//
//  World_StateController.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.App_Start;

namespace GameOfLife.Controllers
{
    public class ClassController : ApiController
    {
        ClassRepository classRepository = null;
        public ClassController()
        {
            this.classRepository = new ClassRepository();
        }

        public HttpResponseMessage Get(HttpRequestMessage request, int? id = null)
        {
            int class_id = ((id != null) ? id.Value : -1);
            Dictionary<string, dynamic> parms = request.GetParams();

            Class[] clsList = null;
            Class cls = null;

            clsList = this.classRepository.GetClasses(class_id);

            if (clsList != null)
            {
                if (class_id > 0)
                {
                    cls = clsList[0];
                    return request.CreateResponse(HttpStatusCode.OK, cls);
                }
                else
                {
                    return request.CreateResponse(HttpStatusCode.OK, clsList);
                }
            }
            else
            {
                return request.CreateResponse(HttpStatusCode.NotFound, clsList);
            }
        }
    }
}
