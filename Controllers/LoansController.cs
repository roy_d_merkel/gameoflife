﻿//  Author:
//       Joe Merkel <merkeljoe9@gmail.com>
//
//  Copyright (c) 2018 Joe Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Http;
using System.Web.SessionState;
using System.Net;
using System.Net.Http;

using GameOfLife.Models;
using GameOfLife.Entities;
using GameOfLife.Filters;
using System.Threading;
using GameOfLife.App_Start;

namespace GameOfLife.Controllers
{
    public class LoansController : ApiController
    {
        LoanRepository loanRepository = null;
        public LoansController()
        {
            this.loanRepository = new LoanRepository();
        }

        [TokenAuthFilter(true, true)]
        public HttpResponseMessage Get(HttpRequestMessage request, int? studentId = null)
        {
            Dictionary<string, dynamic> parms = request.GetParams();

            // Get our session.
            UserSessionIdentity identity = null;
            Session session = null;

            if (Thread.CurrentPrincipal != null && Thread.CurrentPrincipal.Identity != null && Thread.CurrentPrincipal.Identity is UserSessionIdentity)
            {
                identity = (UserSessionIdentity)(Thread.CurrentPrincipal.Identity);

                if (identity.session != null)
                {
                    session = identity.session;
                }
            }

            // If we are not logged in, 404.
            if (session == null)
            {
                return request.CreateResponse(HttpStatusCode.Unauthorized);
            }

            // Use the user's user id, or the supplied ID if we are a teacher or admin.
            if (studentId == null || session.student_id >= 0)
            {
                studentId = session.student_id;
            }

            // Return the expenses.
            List<Loan> loans;
            if (parms.ContainsKey("month"))
            {
                object month;

                parms.TryGetValue("month", out month);

                loans = loanRepository.loans((int)studentId, (DateTime)month);
            }
            else if (parms.ContainsKey("start"))
            {
                if (parms.ContainsKey("end"))
                {
                    object start;
                    object end;

                    parms.TryGetValue("start", out start);
                    parms.TryGetValue("end", out end);

                    loans = loanRepository.loans((int)studentId, (DateTime)start, (DateTime)end);
                }
                else
                {
                    object month;

                    parms.TryGetValue("start", out month);

                    loans = loanRepository.loans((int)studentId, (DateTime)month);
                }
            }
            else
            {
                loans = loanRepository.loans((int)studentId);
            }

            return request.CreateResponse(HttpStatusCode.OK, loans);
        }
    }
}
