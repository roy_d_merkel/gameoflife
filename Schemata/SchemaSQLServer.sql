﻿--
--  SchemaSQLServer.sql
--
--  Author:
--       Roy Merkel <merkel-roy@comcast.net>
--
--  Copyright (c) 2018 Roy Merkel
--
--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.
IF EXISTS(select * from sys.databases where name='GameOfLife')
    DROP DATABASE GameOfLife
GO

CREATE DATABASE GameOfLife

GO

USE GameOfLife

GO

CREATE TABLE Students ( student_id INT NOT NULL IDENTITY(1, 1) PRIMARY KEY, 
                        first_name VARCHAR(50) NOT NULL DEFAULT '',
                        last_name VARCHAR(50) NOT NULL DEFAULT '',
                        user_name VARCHAR(50) NOT NULL DEFAULT '',
                        password VARCHAR(128) NOT NULL DEFAULT '');

CREATE TABLE Teachers ( teacher_id INT NOT NULL IDENTITY(1, 1) PRIMARY KEY, 
                        first_name VARCHAR(50) NOT NULL DEFAULT '',
                        last_name VARCHAR(50) NOT NULL DEFAULT '',
                        user_name VARCHAR(50) NOT NULL DEFAULT '',
                        password VARCHAR(128) NOT NULL DEFAULT '');

CREATE TABLE Class (    class_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY, 
                        teacher_id INT NOT NULL DEFAULT -1,
                        name VARCHAR(100) NOT NULL DEFAULT '',
                        description VARCHAR(100) NOT NULL DEFAULT '');

CREATE TABLE MarketStrength (market_strength_id INT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
                                market_strength INT NOT NULL DEFAULT -1,
                                description VARCHAR(255) NOT NULL DEFAULT '');

CREATE TABLE WorldState (   class_id INT NOT NULL DEFAULT -1, 
                            date_start DATE NOT NULL, 
                            date_end DATE NOT NULL,
                            market_strength_id INT NOT NULL DEFAULT -1,
                            PRIMARY KEY (class_id, date_start, date_end)
);

IF ((SELECT COUNT(*) from fn_listextendedproperty('MS_Description', 
    'SCHEMA', N'dbo', 
    'TABLE', N'MarketStrength', 
    'COLUMN', N'market_strength')) > 0) 
EXEC sp_updateextendedproperty  @name = N'MS_Description', 
                                @value = N'Market Strength from 0 (terrible) to 100 (very strong)', 
                                @level0type = 'SCHEMA', 
                                @level0name = N'dbo', 
                                @level1type = 'TABLE', @level1name = N'MarketStrength', 
                                @level2type = 'COLUMN', @level2name = N'market_strength';
ELSE
EXEC sp_addextendedproperty     @name = N'MS_Description', 
                                @value = N'Market Strength from 0 (terrible) to 100 (very strong)', 
                                @level0type = 'SCHEMA', @level0name = N'dbo', 
                                @level1type = 'TABLE', @level1name = N'MarketStrength', 
                                @level2type = 'COLUMN', @level2name = N'market_strength';

CREATE TABLE Session (  session_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
                        student_id INT NOT NULL DEFAULT -1, 
                        teacher_id INT NOT NULL DEFAULT -1,
                        class_id   INT NOT NULL DEFAULT -1,
                        start_time DATETIME,
                        end_time   DATETIME,
                        token      VARCHAR(128) NOT NULL DEFAULT '');

CREATE TABLE LoginAttempt(  login_attempt_id INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
                            ip VARCHAR(255),
                            user_agent VARCHAR(255),
                            referrer VARCHAR(255),
                            user_name VARCHAR(50),
                            password VARCHAR(128),
                            attempted_on DATETIME,
                            successful BIT DEFAULT 0,
                            is_admin BIT DEFAULT 0);

CREATE TABLE StudentsClasses(   class_id INT NOT NULL DEFAULT -1,
                                student_id INT NOT NULL DEFAULT -1,
                                PRIMARY KEY (class_id, student_id));

CREATE TABLE StudentsFlags ( student_id INT NOT NULL,
                             flag VARCHAR(45) NOT NULL,
                             value VARCHAR(50) DEFAULT '',
                             PRIMARY KEY (student_id, flag));

CREATE TABLE StudentsEvents ( student_id INT NOT NULL,
                              event_id VARCHAR(45) NOT NULL,
                              last_attempted DATE,
                              PRIMARY KEY (student_id, event_id));

GO
