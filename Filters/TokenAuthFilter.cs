﻿//
//  TokenAuthFilter.cs
//
//  Author:
//       Roy Merkel <merkel-roy@comcast.net>
//
//  Copyright (c) 2018 Roy Merkel
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using GameOfLife.Entities;
using GameOfLife.Models;

namespace GameOfLife.Filters
{
    public class TokenAuthFilter : AuthorizationFilterAttribute
    {
        bool isActive;
        bool isMandatory;
        SessionRepository sessionRepository;
        UserRepository userRepository;

        public TokenAuthFilter() : base()
        {
            isActive = true;
            isMandatory = true;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

        public TokenAuthFilter(bool isActive) : base()
        {
            this.isActive = isActive;
            isMandatory = true;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

        public TokenAuthFilter(bool isActive, bool isMandatory) : base()
        {
            this.isActive = isActive;
            this.isMandatory = isMandatory;
            sessionRepository = new SessionRepository();
            userRepository = new UserRepository();
        }

        public string GetToken(HttpActionContext actionContext)
        {
            // get the token from the header
            string token = null;

            IEnumerable<string> values = null;

            if (actionContext.Request.Headers.TryGetValues("Token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
            else if (actionContext.Request.Headers.TryGetValues("token", out values))
            {
                IEnumerator<string> enumerator = values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    token = enumerator.Current;
                }
            }
            // if that failes try the get parameters.
            else
            {
                IEnumerable<KeyValuePair<string, string>> enumerable = actionContext.Request.GetQueryNameValuePairs();
                IEnumerator<KeyValuePair<string, string>> enumerator = enumerable.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    KeyValuePair<string, string> kv = enumerator.Current;

                    if (kv.Key.Equals("Token") || kv.Key.Equals("token"))
                    {
                        token = kv.Value;
                    }
                }
            }

            return token;
        }

		public override void OnAuthorization(HttpActionContext actionContext)
		{
            base.OnAuthorization(actionContext);

            if (!isActive) return;

            string token = GetToken(actionContext);

            if (token == null)
            {
                if (isMandatory)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                return;
            }

            // check for the session.
            Session s = sessionRepository.GetSession(token);

            if (s == null)
            {
                if (isMandatory)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                return;
            }

            // check for session expiration.
            if (s.start_time == null || s.end_time != null || DateTime.Compare(s.start_time.Value, DateTime.Now.Subtract(TimeSpan.FromMinutes(60))) < 0)
            {
                if (isMandatory)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                return;
            }

            //
            GenericIdentity identity = null;
            if (s.teacher_id > 0)
            {
                identity = new UserSessionIdentity(userRepository.GetTeacher(s.teacher_id), s);
                Thread.CurrentPrincipal = new GenericPrincipal(identity, null);
            }
            else if (s.student_id > 0)
            {
                identity = new UserSessionIdentity(userRepository.GetStudent(s.student_id), s);
                Thread.CurrentPrincipal = new GenericPrincipal(identity, null);
            }
            else
            {
                if (isMandatory)
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                }
                return;
            }
		}
	}
}
